global.__base = __dirname

const express = require('express')
const path = require('path')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const sassMiddleware = require('node-sass-middleware')
const database = require('./controllers/database')
const expressSession = require('express-session')
const session = require('./controllers/session')

const app = express()

// conectando a la base de datos
database.connect()
  .catch(err => {
    console.error(err)
    process.exit(1)
  })

// sessiones
app.use(expressSession({
  secret: 'evaluacion-carreras',
  resave: false,
  saveUninitialized: false,
  // cookie: { secure: true }
}))

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')
app.locals.basedir = path.join(__dirname, 'views')

app.use(sassMiddleware({
  src: `${__dirname}/public/scss`,
  dest: path.join(__dirname, 'public/stylesheets'),
  debug: false,
  outputStyle: 'compressed',
  prefix:  '/stylesheets'
}))

app.use('/public', express.static(path.join(__dirname, 'public')))
app.use('/public', express.static(path.join(__dirname, 'node_modules')))

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/rest', require('./routes/rest'))
app.use('/admin', session.admin, require('./routes/admin'))
app.use('/responsable', session.responsable, require('./routes/responsable'))
app.use('/evaluators', session.evaluators, require('./routes/evaluators'))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
