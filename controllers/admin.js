const database = require('./database')
const debug = require('debug')('app:controllers:admin')

exports.index = function (req, res) {
  let data = { user: req.session.admin }
  res.render('admin/index', data)
}

exports.login = {
  get: function (req, res) {
    res.render('admin/login')
  },
  post: function (req, res) {
    let collection = database.getCollection(database.collections.ADMINS)
    collection.findOne(req.body)
      .then(doc => {
        if (doc) {
          req.session.admin = {
            _id: doc._id,
            username: doc.username,
            name: doc.name,
          }
        }
        res.status(doc ? 200 : 404).end()
      })
      .catch(err => {
        console.error('Login post.', err)
        res.status(500).send(err.message)
      })
  }
}

exports.logout = function (req, res) {
  delete req.session.admin
  res.redirect('/admin/login')
}

exports.configuration = function (req, res) {
  let section = req.query.section
  let data = { user: req.session.admin }

  switch (section) {
  case 'charges':
    data.charges = 'active'
    break
  case 'criteria':
    data.criteria = 'active'
    break
  case 'subcriteria':
    data.subcriteria = 'active'
    break
  case 'indicators':
    data.indicators = 'active'
    break
  case 'departments':
    data.departments = 'active'
    break
  case 'people':
    data.people = 'active'
    break
  case 'responsables':
    data.responsables = 'active'
    break
  case 'evaluators':
    data.evaluators = 'active'
    break
  case 'formulas':
    data.formulas = 'active'
    break
  case 'scales':
    data.scales = 'active'
    break
  default:
    data.alerts = 'active'
    break
  }

  res.render('admin/configuration', data)
}
