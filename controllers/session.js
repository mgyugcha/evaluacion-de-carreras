const Users = require('../models/users')
const debug = require('debug')('app:controllers:session')

exports.admin = function (req, res, next) {
  middleware(req, res, next, 'admin')
}

exports.responsable = function (req, res, next) {
  middleware(req, res, next, 'responsable')
}

exports.evaluators = function (req, res, next) {
  middleware(req, res, next, 'evaluators')
}

function middleware(req, res, next, type) {
  let url = req.originalUrl
  if (req.session[type]) {
    if (url.includes(`/${type}/login`)) {
      res.redirect(`/${type}`)
    } else {
      next()
    }
  } else {

    // SESIONES POR DEFECTO
    if (type === 'admin') {
      req.session.admin = {
        _id: '5953e4deef01e071c07fa5a3',
        username: 'mgyugcha',
        name: 'Michael Yugcha (bot)',
      }
      next()
      return
    } else if (type === 'responsable') {
      req.session.responsable = {
        _id: '597d0ec96ac1fe40e259798a',
        responsable_id: '597d10483265f8436c1c9b0e',
        id: '1722430764',
        name: 'Michael',
        surname: 'Bot',
      }
      next()
      return
    } else if (type === 'evaluators') {
      req.session.evaluators = {
        _id: '597d0df36ac1fe40e2597988',
        evaluator_id: '597d10543265f8436c1c9b0f',
        id: '1722430764',
        name: 'Michael',
        surname: 'Bot',
      }
      next()
      return
    }

    debug('%s: no se ha iniciado sesión', type)
    if (url.includes(`/${type}/login`)) {
      debug('la url incluye')
      next()
    } else {
      res.redirect(`/${type}/login`)
    }
  } 
}

// exports.session = function (req, res, next) {
//   let url = req.originalUrl
//   if (req.session.user) {
//     if (url.includes('/login') || url.includes('/signup')) {
//       res.redirect('/')
//     } else {
//       next()
//     }
//   } else {
//     debug('no se ha iniciado sesión')
//     if (url.includes('/login') || url.includes('/signup')) {
//       debug('la url  incluye')
//       next()
//     } else {
//       res.redirect('/login')
//     }
      
//   }
// }

// exports.userLogin = function (user, session) {
//   return new Promise((resolve, reject) => {
//     Users.get(user)
//       .then(data => {
//         if (data) {
//           session.user = getSessionUserData(data)
//           resolve(true)
//         } else {
//           resolve(false)
//         }
//       })
//       .catch(err => { reject(err) })
//   })
// }

function getSessionUserData(data) {
  return {
    '_id': data.objectID,
    'name': data.name,
  }
}
