const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://127.0.0.1/evaluacion-carreras'

var state = {
  database: null,
}

exports.collections = {
  ADMINS: 'admins',
  CRITERIA: 'criteria',
  SUBCRITERIA: 'subcriteria',
  INDICATORS: 'indicators',
  DEPARTMENTS: 'departments',
  ALERT: 'alerts',
  CHARGES: 'charges',
  PEOPLE: 'persons',
  RESPONSABLES: 'responsables',
  EVALUATORS: 'evaluators',
  FORMULAS: 'formulas',
  EVIDENCES: 'evidences',
  SCALES: 'scales',
}

exports.connect = function () {
  return new Promise((resolve, reject) => {
    if (state.database) return resolve()
    MongoClient.connect(url)
      .then(database => {
        state.database = database
        resolve()
      })
      .catch(err => { reject(err) })
  })
}

exports.get = function() {
  return state.database
}

exports.getCollection = function (collectionName) {
  return state.database.collection(collectionName)
}

// exports.close = function () {
//   if (state.db) {
//     state.db.close(function(err, result) {
//       state.db = null
//       state.mode = null
//       done(err)
//     })
//   }
// }
