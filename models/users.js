const ObjectID = require('mongodb').ObjectID
const database = require('../controllers/database')
const get = require('../controllers/utils').getProperty
const maincollection = database.collections.USERS

exports.count = function (query) {
  let collection = database.getCollection(maincollection)
  return collection.find(query, { _id: 1 }).count()
}

exports.get = function (query) {
  let collection = database.get().collection(maincollection)
  return collection.findOne(query)
}

exports.insert = function (user) {
  let collection = database.get().collection(maincollection)

  let schema = {
    _id: get(user, '_id'),
    objectID: new ObjectID(),
    password: get(user, 'password'),
    name: get(user, 'name'),
    email: get(user, 'email'),
  }

  return collection.insertOne(schema)
}

exports.checkUserAndPassword = function (username, password) {
  
}
