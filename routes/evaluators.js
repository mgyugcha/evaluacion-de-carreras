const database = require(__base + '/controllers/database')
const debug = require('debug')('app:routes:evaluators')
const ObjectID = require('mongodb').ObjectID
const router = require('express').Router()

router.get('/', (req, res) => {
  let data = { user: req.session.evaluators }
  res.render('evaluators/index', data)
})

router.get('/calificacion/:id', (req, res) => {
  let data = {
    user: req.session.evaluators,
    responsable_id: req.params.id,
  }
  res.render('evaluators/calificacion', data)
})

router.route('/login')
  .get((req, res) => {
    res.render('evaluators/login')
  })
  .post((req, res) => {
    let collection = database.getCollection(database.collections.PEOPLE)
    let query = {
      'cedula': req.body.id,
      'password': req.body.password,
    }
    var persona = null
    collection.findOne(query)
      .then(doc => {
        if (doc) {
	  persona = doc
	  let evaluatorsCol = database.getCollection(database.collections.EVALUATORS)
	  let query = { 'person_id': new ObjectID(doc._id)}
	  return evaluatorsCol.findOne(query)
        } else {
	  return null
	}
      })
      .then(doc => {
	if (doc) {
	  req.session.evaluators = {
            _id: persona._id,
	    evaluator_id: doc._id,
            id: persona.cedula,
            name: persona.nombres,
	    surname: persona.apellidos,
          }
	}
	res.status(doc ? 200 : 404).end()
      })
      .catch(err => {
        console.error('Login post.', err)
        res.status(500).send(err.message)
      })
  })

router.get('/logout', (req, res) => {
  delete req.session.evaluators
  res.redirect('/evaluators/login')
})

module.exports = router
