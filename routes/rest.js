const router = require('express').Router()

router.use('/departments', require('./rest/departments'))
router.use('/criteria', require('./rest/criteria'))
router.use('/indicators', require('./rest/indicators'))
router.use('/subcriteria', require('./rest/subcriteria'))
router.use('/alerts', require('./rest/alerts'))
router.use('/charges', require('./rest/charges'))
router.use('/people', require('./rest/people'))
router.use('/formulas', require('./rest/formulas'))
router.use('/scales', require('./rest/scales'))
router.use('/evidence', require('./rest/evidence'))
router.use('/responsables', require('./rest/responsables'))
router.use('/evaluators', require('./rest/evaluators'))

module.exports = router
