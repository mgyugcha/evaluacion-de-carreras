const router = require('express').Router()
const database = require(__base +'/controllers/database')
const ObjectID = require('mongodb').ObjectID

router.route('/')
  .get((req, res) => {
    let collection = database.getCollection(database.collections.CRITERIA)
    let query = { archive: { $exists: false }}
    let project = { subcriteria: 0 }
    collection.find(query, project).toArray()
      .then(docs => { res.json(docs) })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })
  .post((req,res) => {
    let collection = database.getCollection(database.collections.CRITERIA)
    collection.insert(req.body)
      .then(() => { res.end() })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })

router.route('/subcriteria')
  .get((req, res) => {
    let collection = database.getCollection(database.collections.CRITERIA)
    let query = { archive: { $exists: false }}
    let project = { 'subcriteria.indicators': 0 }
    collection.find(query, project).toArray()
      .then(docs => { res.json(docs) })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })

router.route('/:id')
  .put((req,res) => {
    let filter = { '_id': new ObjectID(req.params.id) }
    let update = {$set: req.body}
    delete req.body._id
    let collection = database.getCollection(database.collections.CRITERIA)
    collection.updateOne(filter, update)
      .then(() => { res.end() })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      }) 
  })
  .delete((req, res) => {
    let filter = { '_id': new ObjectID(req.params.id) }
    let update = {
      $set: {
        archive : {
          user_id: new ObjectID(req.session.admin._id),
          date: new Date(),
        }
      }
    }
    let collection = database.getCollection(database.collections.CRITERIA)
    collection.updateOne(filter, update)
      .then(() => { res.end() })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })

router.route('/:id/subcriteria')
  .get((req, res) => {
    let collection = database.getCollection(database.collections.CRITERIA)
    let query = {
      _id: new ObjectID(req.params.id),
      archive: { $exists: false },
    }
    let project = { subcriteria: 1 }
    collection.find(query, project).toArray()
      .then(docs => { res.json(docs) })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })
  .post((req, res) => {
    req.body._id = new ObjectID()
    let filter = { '_id': new ObjectID(req.params.id) }
    let update = { $push: { subcriteria: req.body } }
    let collection = database.getCollection(database.collections.CRITERIA)
    console.log(filter, update)
    collection.updateOne(filter, update)
      .then(result => {
        res.end()
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })

router.route('/:id/subcriteria/:subid/indicators')
  .post((req, res) => {
    req.body._id = new ObjectID()
    let filter = {
      '_id': new ObjectID(req.params.id),
      'subcriteria._id': new ObjectID(req.params.subid)
    }
    let update = { $push: { 'subcriteria.$.indicators': req.body } }
    let collection=database.getCollection(database.collections.CRITERIA)
    collection.updateOne(filter, update)
      .then(result => {
        res.end()
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })

module.exports = router
