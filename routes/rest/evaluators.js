const router = require('express').Router()
const database = require(__base +'/controllers/database')
const ObjectID = require('mongodb').ObjectID
const maincollection = database.collections.EVALUATORS
const peoplecollection = database.collections.PEOPLE

router.route('/')
  .get((req, res) => {
    let collection = database.getCollection(maincollection)
    let query = { archive: { $exists: false } }
    collection.aggregate([
      { $match: query },
      { '$unwind': {'path': '$responsables', preserveNullAndEmptyArrays: true } },
      {
        "$lookup": {
          "from": database.collections.RESPONSABLES,
          "localField": "responsables",
          "foreignField": "_id",
          "as": "responsable"
        }
      },
      { "$unwind": { path: "$responsable",  preserveNullAndEmptyArrays: true }},
      // responsa responsable
      {
        $lookup: {
          from: peoplecollection,
          localField: 'responsable.person_id',
          foreignField: '_id',
          as: 'responsable.person',
        }
      },
      { $unwind: { path: "$responsable.person",  preserveNullAndEmptyArrays: true }},
      {
        "$group": {
          "_id": "$_id",
          "person_id": {$first: '$person_id'},
          "responsables": { "$push": "$responsable" }
        }
      },
      {
        $lookup: {
          from: peoplecollection,
          localField: 'person_id',
          foreignField: '_id',
          as: 'person',
        }
      },
      { $unwind: '$person'},
    ]).toArray()
      .then(docs => {
        res.json(docs)
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })
  .post((req,res) => {
    let collection = database.getCollection(maincollection)
    req.body.person_id = new ObjectID(req.body.person_id)
    req.body.responsables = []
    collection.insert(req.body)
      // .then(() => {
      //   let peopleCol = database.getCollection(peoplecollection)
      //   let filter = { '_id': new ObjectID(req.body.person_id) }
      //   let update = { $set: { responsable: true }}
      //   return peopleCol.updateOne(filter, update)
      // })
      .then(() => {
        res.end()
      })
      .catch(err =>{
        console.error(err)
        res.status(500).send(err.message)
      })
  })

router.route('/add-responsable')
  .put((req, res) => {
    let filter = { '_id': new ObjectID(req.body._id) }
    let update = { $push: { responsables: new ObjectID(req.body.responsable_id) } }
    let collection = database.getCollection(maincollection)
    collection.updateOne(filter, update)
      .then(() => { res.end() })
      .catch(err =>{
        console.error(err)
        res.status(500).send(err.message)
      })
  })

// router.route('/:id')
//   .put((req,res) => {
//     let filter = {
//       '_id': new ObjectID(req.params.id)
//     }
//     let update = {$set: req.body}
//     delete req.body._id
//     let collection=database.getCollection(database.collections.PEOPLE)
//     collection.updateOne(filter, update)
//       .then(() =>{
//         res.end()
//       })
//       .catch(err => {
//         console.error(err)
//         res.status(500).send(err.message)
//       }) 
//   })
//   .delete((req, res) => {
//     let filter = { '_id': new ObjectID(req.params.id) }
//     let update = {
//       $set: {
//         archive : {
//           user_id: new ObjectID(req.session.admin._id),
//           date: new Date(),
//         }
//       }
//     }
//     let collection=database.getCollection(database.collections.PEOPLE)
//     collection.updateOne(filter, update)
//       .then(result =>{
//         res.send(result)
//       })
//       .catch(err => {
//         console.error(err)
//         res.status(500).send(err.message)
//       }) 
//   })

router.get('/:id', (req, res) => {
  let collection = database.getCollection(maincollection)
  let query = { _id: new ObjectID(req.params.id), archive: { $exists: false } }
  collection.aggregate([
{ $match: query },
      { '$unwind': {'path': '$responsables', preserveNullAndEmptyArrays: true } },
      {
        "$lookup": {
          "from": database.collections.RESPONSABLES,
          "localField": "responsables",
          "foreignField": "_id",
          "as": "responsable"
        }
      },
      { "$unwind": { path: "$responsable",  preserveNullAndEmptyArrays: true }},
      // responsa responsable
      {
        $lookup: {
          from: peoplecollection,
          localField: 'responsable.person_id',
          foreignField: '_id',
          as: 'responsable.person',
        }
      },
      { $unwind: { path: "$responsable.person",  preserveNullAndEmptyArrays: true }},
      {
        "$group": {
          "_id": "$_id",
          "person_id": {$first: '$person_id'},
          "responsables": { "$push": "$responsable" }
        }
      },
      {
        $lookup: {
          from: peoplecollection,
          localField: 'person_id',
          foreignField: '_id',
          as: 'person',
        }
      },
      { $unwind: '$person'},
  ]).toArray()
    .then(docs => {
      res.json(docs[0])
    })
    .catch(err => {
      console.error(err)
      res.status(500).send(err.message)
    })
})

router.delete('/:evaluatorID/responsables/:responsableID', (req, res) => {
  let query = { '_id': new ObjectID(req.params.evaluatorID) }
  let remove = { $pull: { 'responsables': new ObjectID(req.params.responsableID) } }
  let collection = database.getCollection(maincollection)
  collection.update(query, remove)
    .then(() => { res.end() })
    .catch(err => {
      console.error(err.message)
      res.status(500).send(err.message)
    })
})


module.exports = router
