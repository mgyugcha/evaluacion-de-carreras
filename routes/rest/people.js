const router = require('express').Router()
const database = require(__base +'/controllers/database')
const ObjectID = require('mongodb').ObjectID

router.route('/')
  .get((req, res) => {
    let collection = database.getCollection(database.collections.PEOPLE)
    collection.find({archive: { $exists: false }}).toArray()
      .then(docs => {
        res.json(docs)
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })
  .post((req,res) => {
    let collection = database.getCollection(database.collections.PEOPLE)
    req.body.password = '12345'
    req.body.responsable = false
    collection.insert(req.body)
      .then(() => {
        res.end()
      })
      .catch(err =>{
        console.error(err)
        res.status(500).send(err.message)
      })
  })

router.route('/:id')
  .put((req,res) => {
    let filter = {
      '_id': new ObjectID(req.params.id)
    }
    let update = {$set: req.body}
    delete req.body._id
    let collection=database.getCollection(database.collections.PEOPLE)
    collection.updateOne(filter, update)
      .then(() =>{
        res.end()
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      }) 
  })
  .delete((req, res) => {
    let filter = { '_id': new ObjectID(req.params.id) }
    let update = {
      $set: {
        archive : {
          user_id: new ObjectID(req.session.admin._id),
          date: new Date(),
        }
      }
    }
    let collection=database.getCollection(database.collections.PEOPLE)
    collection.updateOne(filter, update)
      .then(result =>{
        res.send(result)
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      }) 
  })

router.route('/responsables-disponibles')
  .get((req, res) => {
    let query = { archive: { $exists: false }}
    let responsablesCol = database.getCollection(database.collections.RESPONSABLES)

    responsablesCol.aggregate([
      { $match: query },
      {
	$group: { _id: null, people: { $push:  '$person_id' } }
      },
    ]).toArray()
      .then(docs => {
	let collection = database.getCollection(database.collections.PEOPLE)
	return collection.find({
	  archive: { $exists: false },
	  _id: { $nin: docs[0] ? docs[0].people : [] },
	}).toArray()
      })
      .then(docs => {
	res.json(docs)
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })

    // let collection = database.getCollection(database.collections.PEOPLE)
    // collection.find({responsable: false, archive: { $exists: false }}).toArray()
    //   .then(docs => {
    //     res.json(docs)
    //   })
    //   .catch(err => {
    //     console.error(err)
    //     res.status(500).send(err.message)
    //   })
  })

router.route('/evaluators-disponibles')
  .get((req, res) => {
    let query = { archive: { $exists: false }}
    let evaluatorsCol = database.getCollection(database.collections.EVALUATORS)

    evaluatorsCol.aggregate([
      { $match: query },
      {
	$group: { _id: null, people: { $push:  '$person_id' } }
      },
    ]).toArray()
      .then(docs => {
	let collection = database.getCollection(database.collections.PEOPLE)
	return collection.find({
	  archive: { $exists: false },
	  _id: { $nin: docs[0] ? docs[0].people : [] },
	}).toArray()
      })
      .then(docs => {
	res.json(docs)
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })

module.exports = router
