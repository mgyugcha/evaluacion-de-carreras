const router = require('express').Router()
const database = require(__base +'/controllers/database')
const ObjectID = require('mongodb').ObjectID
const subcriteriacol = database.collections.SUBCRITERIA
const criteriacol = database.collections.CRITERIA
const maincollection = database.collections.INDICATORS
const formulascol = database.collections.FORMULAS

router.route('/')
  .get((req, res) => {
    let collection = database.getCollection(maincollection)
    let query = { archive: { $exists: false }}
    collection.aggregate([
      { $match: query },
      {
        $lookup: {
          from: subcriteriacol,
          localField: 'subcriterion_id',
          foreignField: '_id',
          as: 'subcriterion',
        }
      },
      { $unwind: '$subcriterion'},
      {
        $match: { 'subcriterion.archive': { $exists: false} }
      },
      // {
      //   $addFields: { 'subcriterion_id': '$subcriterion._id' }
      // },
      {
        $lookup: {
          from: criteriacol,
          localField: 'subcriterion.criterion_id',
          foreignField: '_id',
          as: 'criterion',
        }
      },
      { $unwind: '$criterion' },
      {
        $match: { 'criterion.archive': { $exists: false} }
      },
      // formulas
      {
        $lookup: {
          from: formulascol,
          localField: 'formula_id',
          foreignField: '_id',
          as: 'formula',
        }
      },
      { "$unwind": { path: "$formula",  preserveNullAndEmptyArrays: true }},
      // { $unwind: '$formula' },
    ]).toArray()
      .then(docs => { res.json(docs) })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })
  .post((req,res) => {
    let collection = database.getCollection(maincollection)
    req.body.subcriterion_id = new ObjectID(req.body.subcriterion_id)
    req.body.formula_id = new ObjectID(req.body.formula_id)
    collection.insert(req.body)
      .then(() => { res.end() })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })

router.get('/get-reports', (req, res) => {
  let collection = database.getCollection(database.collections.INDICATORS)
  let query = { archive: { $exists: false }}

  collection.aggregate([
    { $match: query },
    {
      $lookup: {
        from: database.collections.SCALES,
        localField: 'calificacion.scale_id',
        foreignField: '_id',
        as: 'scales',
      }
    },
    { "$unwind": { path: "$scales",  preserveNullAndEmptyArrays: true }},
    // {
    //   $group: {
    //     _id: '$scales._id',
    //     //indicadores: { $push: "$$ROOT" },
    //     count: { $sum: 1 }
    //   }
    // }
  ]).toArray()
    .then(docs => { res.json(docs) })
    .catch(err => {
      console.error(err)
      res.status(500).send(err.message)
    })
})

router.route('/:id')
  .put((req,res) => {
    let filter = { '_id': new ObjectID(req.params.id) }
    req.body.subcriterion_id = new ObjectID(req.body.subcriterion_id)
    console.log(req.body.formula_id)
    if (req.body.formula_id)
      req.body.formula_id = new ObjectID(req.body.formula_id)
    let update = {$set: req.body}
    if (!req.body.formula_id) {
      update['$unset'] = { formula_id: "" }
    }

    delete req.body._id
    delete req.body.subcriterion
    delete req.body.criterion
    delete req.body.formula
    let collection = database.getCollection(maincollection)
    collection.updateOne(filter, update)
      .then(() => { res.end() })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      }) 
  })
  .delete((req, res) => {
    let filter = { '_id': new ObjectID(req.params.id) }
    let update = {
      $set: {
        archive : {
          user_id: new ObjectID(req.session.admin._id),
          date: new Date(),
        }
      }
    }
    console.log(filter, update)
    let collection = database.getCollection(maincollection)
    collection.updateOne(filter, update)
      .then(() => { res.end() })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  }) 

router.get('/disponibles', (req, res) => {
  let collection = database.getCollection(maincollection)
  let query = { archive: { $exists: false }}

  let responsablesCol = database.getCollection(database.collections.RESPONSABLES)

  responsablesCol.aggregate([
    { $match: query },
    {
      $group: { _id: null, indicadores: { $addToSet:  '$indicators' } }
    },
    {
      "$addFields": {
        "indicadores": {
          "$reduce": {
            "input": "$indicadores",
            "initialValue": [],
            "in": { "$setUnion": [ "$$value", "$$this" ] }
          }
        }
      }
    }
  ]).toArray()
    .then(docs => {
      let query2 = {
        archive: { $exists: false },
        _id: { $nin: docs[0].indicadores },
      }
      return collection.aggregate([
        { $match: query2 },
        {
          $lookup: {
            from: subcriteriacol,
            localField: 'subcriterion_id',
            foreignField: '_id',
            as: 'subcriterion',
          }
        },
        { $unwind: '$subcriterion'},
        {
          $match: { 'subcriterion.archive': { $exists: false} }
        },
        // {
        //   $addFields: { 'subcriterion_id': '$subcriterion._id' }
        // },
        {
          $lookup: {
            from: criteriacol,
            localField: 'subcriterion.criterion_id',
            foreignField: '_id',
            as: 'criterion',
          }
        },
        { $unwind: '$criterion' },
        {
          $match: { 'criterion.archive': { $exists: false} }
        },
        // // formulas
        // {
        //   $lookup: {
        //     from: formulascol,
        //     localField: 'formula_id',
        //     foreignField: '_id',
        //     as: 'formula',
        //   }
        // },
        // { $unwind: '$formula' },
      ]).toArray()
    })
    .then(docs => {
      res.json(docs)
    })
    .catch(err => {
      console.error(err)
      res.status(500).send(err.message)
    })
})

router.post('/:id/calificar', (req, res) => {
  let collection = database.getCollection(maincollection)
  let query = { '_id': new ObjectID(req.params.id) }
  if (req.body.scale_id) {
    req.body.scale_id = new ObjectID(req.body.scale_id)
  }

  let update = {
    $set: {
      'calificacion': req.body
    }
  }
  collection.updateOne(query, update)
    .then(() => {
      res.end()
    })
    .catch(err => {
      res.status(500).send(err.message)
    })
})


module.exports = router
