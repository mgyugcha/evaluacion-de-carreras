const router = require('express').Router()
const fs = require('fs')
const database = require(__base +'/controllers/database')
const fileUpload = require('express-fileupload')
const ObjectID = require('mongodb').ObjectID
const maincollection = database.collections.EVIDENCES
const uploadsdir = __dirname + '/../../public/uploads/'

router.use(fileUpload())

router.get('/get-reports', (req, res) => {
  let collection = database.getCollection(database.collections.EVIDENCES)
  let query = { archive: { $exists: false }}

  collection.aggregate([
    { $match: query },
    {
      $lookup: {
        from: database.collections.ALERT,
        localField: 'calificacion.alert_id',
        foreignField: '_id',
        as: 'alerts',
      }
    },
    { "$unwind": { path: "$alerts",  preserveNullAndEmptyArrays: true }},
    {
      $group: {
        _id: '$alerts._id',
        indicadores: { $push: "$$ROOT" },
        count: { $sum: 1 }
      }
    }
  ]).toArray()
    .then(docs => { res.json(docs) })
    .catch(err => {
      console.error(err)
      res.status(500).send(err.message)
    })
})

router.route('/:id')
  .get((req, res) => {
    let query = { '_id': new ObjectID(req.params.id) }
    let collection = database.getCollection(maincollection)
    collection.findOne(query)
      .then(doc => {
        res.setHeader('Content-disposition', 'attachment; filename=' + doc.name)
        res.setHeader('Content-type', doc.mimetype)
        var filestream = fs.createReadStream(uploadsdir + doc._id)
        filestream.pipe(res)
      })
      .catch(err => {
        console.error(err.message)
        res.status(500).send(err.message)
      })
  })
  .delete((req, res) => {
    let query = { '_id': new ObjectID(req.params.id) }
    let collection = database.getCollection(maincollection)
    collection.deleteOne(query)
      .then(() => {
        fs.unlinkSync(uploadsdir + req.params.id)
        res.end()
      })
      .catch(err => {
        console.error(err.message)
        res.status(500).send(err.message)
      })
  })

router.route('/upload-file')
  .post((req, res) => {
    if (!req.files) return res.status(400).send('No hay archivos')
    let archivo = req.files.archivo
    let id = new ObjectID()

    let evidence = {
      _id: id,
      responsable_id: new ObjectID(req.session.responsable._id),
      indicator_id: new ObjectID(req.query.indicator),
      name: archivo.name,
      mimetype: archivo.mimetype,
      date: new Date(),
    }

    archivo.mv(uploadsdir + id, (err) => {
      if (err) {
        console.error(err)
        return res.status(500).send(err.message)
      }

      let collection = database.getCollection(maincollection)
      collection.insert(evidence)
        .then(() => {
          res.send('Se subio correctamente')
        })
        .catch(err => {
          res.status(500).send(err.message)
        })

    })
  })

router.post('/:id/calificar', (req, res) => {
  let collection = database.getCollection(maincollection)
  let query = { '_id': new ObjectID(req.params.id) }
  let update = {
    $set: {
      'calificacion': {
        alert_id: new ObjectID(req.body.alert_id),
        descripcion: req.body.descripcion
      }
    }
  }
  collection.updateOne(query, update)
    .then(() => {
      res.end()
    })
    .catch(err => {
      res.status(500).send(err.message)
    })
})

module.exports = router
