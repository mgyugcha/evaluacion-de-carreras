const router = require('express').Router()
const database = require(__base +'/controllers/database')
const ObjectID = require('mongodb').ObjectID
const subcriteriacol = database.collections.SUBCRITERIA
const criteriacol = database.collections.CRITERIA

router.route('/')
  .get((req, res) => {
    let collection = database.getCollection(criteriacol)
    let query = { archive: { $exists: false }}
    collection.aggregate([
      { $match: query },
      {
        $lookup: {
          from: subcriteriacol,
          localField: '_id',
          foreignField: 'criterion_id',
          as: 'subcriterion',
        }
      },
      { $unwind: '$subcriterion'},
      {
        $addFields: { 'subcriterion_name': '$subcriterion.subcriterion' }
      },
      {
        $match: { 'subcriterion.archive': { $exists: false} }
      },
    ]).toArray()
      .then(docs => { res.json(docs) })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })
  .post((req,res) => {
    let collection = database.getCollection(subcriteriacol)
    req.body.criterion_id = new ObjectID(req.body.criterion_id)
    collection.insert(req.body)
      .then(() => { res.end() })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })

router.route('/:id')
  .put((req,res) => {
    let filter = { '_id': new ObjectID(req.params.id) }
    req.body.criterion_id = new ObjectID(req.body.criterion_id)
    let update = {$set: req.body}
    delete req.body._id
    let collection = database.getCollection(subcriteriacol)
    collection.updateOne(filter, update)
      .then(() => { res.end() })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      }) 
  })
  .delete((req, res) => {
    let filter = { '_id': new ObjectID(req.params.id) }
    let update = {
      $set: {
        archive : {
          user_id: new ObjectID(req.session.admin._id),
          date: new Date(),
        }
      }
    }
    let collection = database.getCollection(subcriteriacol)
    collection.updateOne(filter, update)
      .then(() => { res.end() })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })

module.exports = router
