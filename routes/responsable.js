const database = require(__base + '/controllers/database')
const debug = require('debug')('app:routes:responsables')
const ObjectID = require('mongodb').ObjectID
const router = require('express').Router()

router.get('/', (req, res) => {
  let data = { user: req.session.responsable }
  res.render('responsable/index', data)
})

router.route('/login')
  .get((req, res) => {
    res.render('responsable/login')
  })
  .post((req, res) => {
    let collection = database.getCollection(database.collections.PEOPLE)
    let query = {
      'cedula': req.body.id,
      'password': req.body.password,
    }
    var persona = null
    collection.findOne(query)
      .then(doc => {
        if (doc) {
	  persona = doc
	  let responsablesCol = database.getCollection(database.collections.RESPONSABLES)
	  let query = { 'person_id': new ObjectID(doc._id)}
	  return responsablesCol.findOne(query)
        } else {
	  return null
	}
      })
      .then(doc => {
	if (doc) {
	  req.session.responsable = {
            _id: persona._id,
	    responsable_id: doc._id,
            id: persona.cedula,
            name: persona.nombres,
	    surname: persona.apellidos,
          }
	}
	res.status(doc ? 200 : 404).end()
      })
      .catch(err => {
        console.error('Login post.', err)
        res.status(500).send(err.message)
      })
  })

router.get('/logout', (req, res) => {
  delete req.session.responsable
  res.redirect('/responsable/login')
})

module.exports = router
