const router = require('express').Router()
const admin = require('../controllers/admin')

router.get('/', admin.index)

router.route('/login')
  .get(admin.login.get)
  .post(admin.login.post)

router.get('/logout', admin.logout)

router.get('/signup', (req, res, next) => {
  res.render('signup')
})

router.get('/configuration', admin.configuration)

module.exports = router
