(function (exports) {
  exports.saved = function (callback) {
    swal({
      title: 'Guardado',
      type: 'success',
      timer: 1500,
      showConfirmButton: false,
      allowEscapeKey: false,
    }, callback)
  }

  exports.formWarning = function () {
    swal({
      title: 'Errores en el formulario',
      text: 'Corriga los errores antes de enviar el formulario',
      type: 'warning',
      confirmButtonClass: 'btn-warning',
      confirmButtonText: 'Aceptar'
    })
  }

  exports.serverError = function () {
    swal({
      title: 'Error en el servidor',
      text: `\
No se pudo realizar la operación. Por favor inténtelo mas tarde`,
      type: 'error',
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Aceptar',
    })
  }





  exports.swal = function (options, callback) {
    swal(options, callback)
  }

  exports.notify = function (title, text, callback) {
    swal({
      title: title,
      text: text,
      confirmButtonText: 'Aceptar',
    }, callback)
  }

  exports.error = function (title, text, callback) {
    swal({
      title: title,
      text: text,
      type: 'error',
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Aceptar',
    }, callback)
  }

  exports.info = function (title, text, callback) {
    swal({
      title: title,
      text: text,
      type: 'info',
      confirmButtonClass: 'btn-info',
      confirmButtonText: 'Aceptar',
    }, callback)
  }

  exports.warning = function (title, text, callback) {
    swal({
      title: title,
      text: text,
      type: 'warning',
      confirmButtonClass: 'btn-warning',
      confirmButtonText: 'Aceptar',
    }, callback)
  }

  exports.success = function (title, text, callback) {
    swal({
      title: title,
      text: text,
      type: 'success',
      timer: 1000,
      showConfirmButton: false,
      allowEscapeKey: false,
    }, callback)
  }

  exports.successPermanent = function (title, text, callback) {
    swal({
      title: title,
      text: text,
      type: 'success',
      confirmButtonClass: 'btn-success',
      confirmButtonText: 'Aceptar'
    }, callback)
  }

  exports.pdfError = function () {
    swal({
      title: 'No se pudo generar PDF',
      text: `
En este momento no se puede generar el PDF. Inténtelo mas tarde.`,
      type: 'error',
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Aceptar',
    })
  }

})(this['notify']={})
