function autocompleteLink($scope, $element) {
  let options = $scope.acOptions
  let id = $scope.acOptions.datasets.display

  // bloodhound

  let bloodhoundOptions = {
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
  }
  Object.assign(bloodhoundOptions, options.bloodhound)
  let source = new Bloodhound(bloodhoundOptions)

  // typeahead
  let typeaheadOptions = options.typeahead || null
  let typeaheadDatasets = { limit: 10, source: source }
  Object.assign(typeaheadDatasets, options.datasets)

  let instance = $element.typeahead(typeaheadOptions, typeaheadDatasets)

  instance.bind('typeahead:select', function (e, selected) {
    $scope.ngModel = selected[id]
    if ($scope.acSelected) $scope.acSelected(selected)
    $scope.$apply()
  })

}

function autocompleteDirective() {
  return {
    //require: 'ngModel',
    scope: {
      acOptions: '=',
      acSelected: '=',
      ngModel: '='
    },
    link: autocompleteLink,
  }
}



//   function getTemplate(tAttrs) {
//     let idModal = tAttrs.idModal
//     let createModal = $(`#${idModal}`).length === 0
//     let modalName = `autocomplete-modal-${idModal}`
//     let template   = `
// <div id="autocomplete" class="input-group">
//   <input id="input-autocomplete" class="typeahead form-control"
//          ng-required="{{required}}" ng-disabled={{disabled}}
//          type="text" ng-model="ngModel" placeholder="{{placeholder}}"
//          ng-change='change(ngModel)'>
//   <span data-toggle="modal" style="cursor:pointer"
//         data-target="#${modalName}" ng-click="openModal()"
//         class="input-group-addon">
//     <span class="glyphicon glyphicon-search" type="button"></span>
//   </span>
// </div>
// `
//     if (!createModal) return template

//     template += `
// <div class="modal fade" id="${modalName}" tabindex="-1" role="dialog">
//   <div class="modal-dialog ${tAttrs.modalClass}" role="document">
//     <div class="modal-content">
//       <div class="modal-header">
//         <button type="button" class="close" data-dismiss="modal"
//                 aria-label="Close"><span aria-hidden="true">&times;</span>
//         </button>
//         <h4 class="modal-title">Busqueda detallada</h4>
//       </div>
//       <div class="modal-body">
//         <input class="form-control" type="text" ng-model="dataFilter"
//                placeholder="Ingrese el término que desea buscar">
//         <div class="table-autocomplete">
//           <table class="table table-hover">
//             <thead>
//               <tr><th ng-repeat="x in modalThead">{{ x }}</th></tr>
//             </thead>
//             <tbody>
//               <tr ng-repeat="x in modalData | filter: dataFilter"
//                   ng-click="selectFromModal(x)" style="cursor:pointer">
//                 <td ng-repeat="y in modalTbody">{{ x[y] }}</td>
//               </tr>
//             </tbody>
//           </table>
//         </div>
//       </div>
//     </div>
//   </div>
// </div>`
//     return template
//   }

//   function autocompleteDirective($http, shared) {
//     return {
//       restrict: 'E',
//       require: 'ngModel',
//       scope: {
//         modalThead: '=',
//         modalTbody: '=',
//         ngModel: '=',
//         modalClass: '@',
//         idModal: '@',
//         placeholder: '@',
//         suggest: '=',
//         key: '@',
//         urlGet: '@',
//         required: '=',
//         disabled: '=',
//         change: '=autocompleteChange',
//         selected: '=autocompleteSelect',
//       },
//       template: function (tElement, tAttrs) {
//         return getTemplate(tAttrs)
//       },
//       link: function link($scope, element, attrs, ngModel) {
//         let input = element.find('#input-autocomplete')
// 	let union = '?'
// 	if (attrs.urlGet.indexOf('?') !== -1) union = '&'
// 	console.log(attrs.urlGet)
//         let source = new Bloodhound({
//           prefetch: attrs.urlGet,
//           queryTokenizer: Bloodhound.tokenizers.whitespace,
//           datumTokenizer: Bloodhound.tokenizers.whitespace,
//           remote: { url: `${attrs.urlGet}${union}q=%QUERY`, wildcard: '%QUERY' },
//         })

//         let instance = input.typeahead(null, {
//           display: attrs.key,
//           source: source,
//           limit: 10,
//           // templates: {
//           //   suggestion: Handlebars.compile(`<div>${$scope.suggest}</div>`)
//           // }
//         })

//         $scope.$watch('ngModel', () => {
//           if ($scope.ngModel)
//             input.typeahead('val', $scope.ngModel)
//         })

//         instance.bind('typeahead:select', function (e, selected) {
//           ngModel.$setViewValue(selected[attrs.key])
// 	  if ($scope.selected)
//             $scope.selected(selected)
//           $scope.$apply()
//         })

//         // Funciones para el autocomplete
//         $scope.openModal = function () {
//           $http.get(attrs.urlGet)
//             .then(res => {
//               let modal = $(`#autocomplete-modal-${attrs.idModal}`)
//               let modalScope = angular.element(modal).scope()
//               modalScope.modalData = res.data
//             })
//           shared.setSelectFunction($scope.selectFunction)
//         }

//         $scope.selectFromModal = function (selected) {
//           $(`#autocomplete-modal-${attrs.idModal}`).modal('hide')
// 	  $(`#autocomplete-modal-${attrs.idModal}`)
// 	    .on('hidden.bs.modal', function (e) {
// 	      shared.getSelectFunction()(selected)
// 	      $scope.$apply()
// 	    })
//         }

//         $scope.selectFunction = function (selected) {
//           input.typeahead('val', selected[attrs.key])
//           ngModel.$setViewValue(selected[attrs.key])
// 	  if ($scope.selected)
//             $scope.selected(selected)
//         }
//       }
//     }
//   }

//   function sharedService() {
//     var selectFunction = null
//     return {
//       setSelectFunction: function (shared) { selectFunction = shared },
//       getSelectFunction: function () { return selectFunction }
//     }
//   }

//   angular.module('TocResources.autocomplete', [])
//     .service('shared', sharedService)
//     .directive('autocomplete', ['$http', 'shared', autocompleteDirective])


angular.module('autocomplete', [])
// .component('autocomplete', autocompleteOptions)
  .directive('autocomplete', autocompleteDirective)
