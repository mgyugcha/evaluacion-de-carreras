let colors = [
  'rgb(245, 171, 53)',
  'rgb(0, 177, 106)',
  'rgb(58, 83, 155)',
  'rgb(231, 76, 60)',
  'rgb(51, 110, 123)'
]

angular.module('main', [])
  .controller('main', ($scope, $http) => {


    $http.get('/rest/evidence/get-reports')
      .then(res => {
	$scope.evidences = res.data
	$scope.graphEvidences()

      }, res => {
	console.error(res.data)
      })

    $scope.graphEvidences = function () {
      let ctx = document.getElementById("chartEvidence").getContext('2d');
      let datasets = []
      let labels = []

      $scope.evidences.forEach(x => {
	if (!x._id) {
	  labels.push('Sin calificar')
	  console.log('asd')
	} else {
	  labels.push(x.indicadores[0].alerts.id)
	}

	datasets.push(x.count)
      })
      

      var myChart = new Chart(ctx, {
	type: 'pie',
	data: {
	  datasets: [{
            data: datasets,
	    backgroundColor: colors,
	  }],
	  labels: labels,
	},
	// options: {
        //   scales: {
        //     yAxes: [{
        //       ticks: {
        //         beginAtZero:true
        //       }
        //     }]
        //   }
	// }
      });
    }

  })
