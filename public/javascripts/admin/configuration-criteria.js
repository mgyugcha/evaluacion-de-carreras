function criteriaController($scope,$http) {
  $scope.criteria = []

  $scope.init = function () {
    $http.get('/rest/criteria')
      .then(res => {
        $scope.criteria = res.data
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.submit = function () {
    let url = '/rest/criteria/' + ($scope.criterion._id || '')
    let http = $scope.criterion._id ? $http.put : $http.post  
    http(url, $scope.criterion)
      .then(res => {
        $('#criteriaModal').modal('hide')
        notify.saved()
        $scope.init()
        delete $scope.criterion
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.delete = function (id) {
    notify.swal({
      title: '¿Borrar?',
      text: 'Los datos se borraran permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete('/rest/criteria/' + id)
        .then(res => {
          notify.success('Borrado')
          $scope.init()
        }, res => {
          console.error(res.data)
          notify.serverError()
        })
    })
  }


  $scope.openModal = function (criterion) {
    $scope.criterion = criterion ? angular.copy(criterion) : {}
    $('#criteriaModal').modal('show')
  }

  $scope.openSubcriteriaModal = function (id, x) {
    $scope.subcriterion = x ? angular.copy(x) : { criterion_id: id }
    $('#subcriteriaModal').modal('show')    
  }

  $scope.submitSubcriterion = function () {
    console.log($scope.subcriterion)
    let url = '/rest/criteria/' + $scope.subcriterion.criterion_id +
        '/subcriteria/' + ($scope.subcriterion._id || '')
    let http = $scope.subcriterion._id ? $http.put : $http.post
    delete $scope.subcriterion.criterion_id
    http(url, $scope.subcriterion)
      .then(res => {
        $('#subcriteriaModal').modal('hide')
        notify.saved()
        // if (!$scope.subcriterion._id) {
        //   $scope.criteria.push(res.data)
        // }
        // else {
        //   $scope.criteria.forEach(x => {
        //     if (x._id === $scope.subcriterion._id){
        //       Object.assign(x, $scope.subcriterion)
        //       return
        //     }
        //   })
        // }
        delete $scope.subcriterion
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.openIndicatorsModal = function (id, subid, x) {
    console.log(id, subid)
    $scope.indicator = x ? angular.copy(x) : {
      criterion_id: id,
      subcriterion_id: subid
    }
    $('#indicatorsModal').modal('show')    
  }

  $scope.submitIndicator = function () {
    console.log($scope.indicator)
    let url = '/rest/criteria/' + $scope.indicator.criterion_id +
        '/subcriteria/' + $scope.indicator.subcriterion_id +
        '/indicators/' + ($scope.indicator._id || '')
    let http = $scope.indicator._id ? $http.put : $http.post
    delete $scope.indicator.criterion_id
    delete $scope.indicator.subcriterion_id
    http(url, $scope.indicator)
      .then(res => {
        $('#indicatorsModal').modal('hide')
        notify.saved()
        // if (!$scope.indicator._id) {
        //   $scope.criteria.push(res.data)
        // }
        // else {
        //   $scope.criteria.forEach(x => {
        //     if (x._id === $scope.indicator._id){
        //       Object.assign(x, $scope.indicator)
        //       return
        //     }
        //   })
        // }
        delete $scope.indicator
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }
}

function subcriteriaController($scope, $http) {
  $scope.criteria = []
  $scope.t = { }

  $scope.v = {
    criterion: {
      class: '',
      selected: function (data) {
        $scope.subcriterion.criterion_id = data._id
        $scope.criterion = `${data.id} · ${data.criterion}`
        $scope.v.criterion.class = 'has-success'
      },
      change: function (value) {
        delete $scope.criterion
        delete $scope.subcriterion.criterion_id
        $scope.v.criterion.class = value ? 'has-warning' : ''
      },
    }
  }

  $scope.acOptions = {
    datasets: {
      display: 'id',
      templates: {
        suggestion: Handlebars.compile(`\
<div><strong>{{id}}</strong> · {{criterion}}</div>`)
      }
    },
    bloodhound: {
      prefetch: '/rest/criteria',
      remote: { url: '/rest/criteria' }
    },
  }

  $scope.init = function () {
    $http.get('/rest/subcriteria')
      .then(res => {
        $scope.criteria = res.data
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.openModal = function (x) {
    $scope.subcriterion = x ? angular.copy(x.subcriterion) : {}
    delete $scope.criterion
    if (x) {
      $scope.v.criterion.class = 'has-success'
      $scope.t.criterion_id = x.criterion
    } else {
      $scope.v.criterion.class = ''
      delete $scope.t.criterion_id
    }
    $('#subcriteriaModal').modal('show')
  }

  $scope.submit = function () {
    if ($scope.v.criterion.class !== 'has-success') {
      notify.formWarning()
      return
    }
    let url = '/rest/subcriteria/'  + ($scope.subcriterion._id || '')
    let http = $scope.subcriterion._id ? $http.put : $http.post
    http(url, $scope.subcriterion)
      .then(res => {
        $('#subcriteriaModal').modal('hide')
        notify.saved()
        $scope.init()
	delete $scope.t.criterion_id
        delete $scope.subcriterion
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.delete = function (id) {
    notify.swal({
      title: '¿Borrar?',
      text: 'Los datos se borraran permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      console.log('/rest/subcriteria/' + id)
      $http.delete('/rest/subcriteria/' + id)
        .then(res => {
          notify.success('Borrado')
          $scope.init()
        }, res => {
          console.error(res.data)
          notify.serverError()
        })
    })
  }
}

function indicatorsController($scope, $http) {
  $scope.indicators = []
  $scope.t = { }

  $scope.v = {
    subcriterion: {
      class: '',
      selected: function (data) {
        $scope.indicator.subcriterion_id = data.subcriterion._id
        $scope.subcriterion = `${data.id}.${data.subcriterion.id} · ${data.subcriterion_name}`
        $scope.v.subcriterion.class = 'has-success'
      },
      change: function (value) {
        delete $scope.subcriterion
        delete $scope.indicator.subcriterion_id
        $scope.v.subcriterion.class = value ? 'has-warning' : ''
      },
    },
    formulas: {
      class: '',
      selected: function (data) {
        $scope.indicator.formula_id = data._id
        $scope.formula = `${data.id} · ${data.formula}`
        $scope.v.formulas.class = 'has-success'
      },
      change: function (value) {
        delete $scope.formula
        delete $scope.indicator.formula_id
        $scope.v.formulas.class = value ? 'has-warning' : ''
      },
    }
  }

  $scope.acOptions = {
    datasets: {
      display: 'subcriterion_name',
      templates: {
        suggestion: Handlebars.compile(`\
<div><strong>{{id}}.{{subcriterion.id}}</strong> · {{subcriterion.subcriterion}}</div>`)
      }
    },
    bloodhound: {
      prefetch: '/rest/subcriteria',
      remote: { url: '/rest/subcriteria' }
    },
  }

  $scope.acOptionsFormulas = {
    datasets: {
      display: 'id',
      templates: {
        suggestion: Handlebars.compile(`\
<div><strong>{{id}}</strong> · {{formula}}</div>`)
      }
    },
    bloodhound: {
      prefetch: '/rest/formulas',
      remote: { url: '/rest/formulas' }
    },
  }

  $scope.init = function () {
    $http.get('/rest/indicators')
      .then(res => {
        $scope.indicators = res.data
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.openModal = function (x) {
    $scope.indicator = x ? angular.copy(x) : {}
    delete $scope.subcriterion
    delete $scope.formula
    if (x) {
      // subcriterion
      $scope.v.subcriterion.class = 'has-success'
      $scope.t.subcriterion_id = x.subcriterion.subcriterion
      // formulas
      if (x.formula) {
	$scope.v.formulas.class = 'has-success'
	$scope.t.formula_id = x.formula.id
      }
    } else {
      $scope.v.subcriterion.class = ''
      delete $scope.t.subcriterion_id
    }
    $('#indicatorsModal').modal('show')
  }

  $scope.submit = function () {
    if ($scope.v.subcriterion.class !== 'has-success') {
      notify.formWarning()
      return
    }
    if ($scope.t.formula_id && ($scope.v.formulas.class !== 'has-success')) {
      notify.formWarning()
      return
    } else {
      delete $scope.indicator.formula_id
    }

    let url = '/rest/indicators/'  + ($scope.indicator._id || '')
    let http = $scope.indicator._id ? $http.put : $http.post
    http(url, $scope.indicator)
      .then(res => {
        $('#indicatorsModal').modal('hide')
        notify.saved()
        $scope.init()
    	delete $scope.t.subcriterion_id
        delete $scope.indicator
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.delete = function (id) {
    notify.swal({
      title: '¿Borrar?',
      text: 'Los datos se borraran permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete('/rest/indicators/' + id)
        .then(res => {
          notify.success('Borrado')
          $scope.init()
        }, res => {
          console.error(res.data)
          notify.serverError()
        })
    })
  }
}

angular.module('configuration')
  .controller('criteria', criteriaController)
  .controller('subcriteria', subcriteriaController)
  .controller('indicators', indicatorsController)
