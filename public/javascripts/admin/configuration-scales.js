function scalesController($scope,$http){
  $scope.scales = []

  $scope.init = function() {
    $http.get('/rest/scales')
    .then(res=>{
      $scope.scales=res.data
    }, res=>{
      console.error(res.data)
      notify.serverError()
    })
  }

  $scope.submit = function () {
    let url = '/rest/scales/' + ($scope.scale._id || '')
    let http = $scope.scale._id ? $http.put : $http.post  
    http(url, $scope.scale)
    .then(res => {
      $('#scalesModal').modal('hide')
      notify.saved()
      $scope.init()
      delete $scope.scale
    }, res => {
      console.error(res.data)
      notify.serverError()
    }) 
  }

  $scope.delete = function (id) {
    notify.swal({
      title: '¿Borrar?',
      text: 'Los datos se borraran permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete('/rest/scales/' + id)
      .then(res => {
        notify.success('Borrado')
        $scope.init()
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
    })
  }

  $scope.openModal = function (scale=null) {
    $scope.scale = scale ? angular.copy(scale) : {}
    $('#scalesModal').modal('show')
  }

}
angular.module('configuration')
  .controller('scales', scalesController)