function peopleController($scope, $http) {
  $scope.people = []

  $scope.init = function () {
    $http.get('/rest/people')
      .then(res => {
        $scope.people = res.data
      }, res=> {
        console.error(res.data)
        notify.serverError()
      }) 
  }

  $scope.openModal = function (person=null) {
    $scope.person = person ? angular.copy(person) : {}
    $('#personModal').modal('show')
  }

  $scope.submit = function () {
    let url = '/rest/people/' + ($scope.person._id || '')
    let http =  $scope.person._id ? $http.put : $http.post
    http(url, $scope.person)
      .then(res => {
        $('#personModal').modal('hide')
        notify.saved()
        $scope.init()
        delete $scope.person
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.delete = function (id) {
    notify.swal({
      title: '¿Borrar?',
      text: 'Los datos se borrarán permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete('/rest/people/' + id)
        .then(res => {
          notify.success('Borrado')
          $scope.init()
        }, res => {
          console.error(res.data)
          notify.serverError()
        })
    })
  }
}

function responsablesController($scope, $http) {

  $scope.responsable = { }

  $scope.v = {
    responsables: {
      class: '',
      selected: function (data) {
        $scope.responsable.person_id = data._id
        $scope.person = `${data.cedula} · ${data.nombres} ${data.apellidos}`
        $scope.v.responsables.class = 'has-success'
      },
      change: function (value) {
        delete $scope.person
        delete $scope.responsable.person_id
        $scope.v.responsables.class = value ? 'has-warning' : ''
      },
    },
    indicator: {
      class: '',
      selected: function (data) {
        $scope.responsable.indicator_id = data._id
        $scope.indicator = `${data.criterion.id}.${data.subcriterion.id}.${data.id} · ${data.indicator}`
        $scope.v.indicator.class = 'has-success'
      },
      change: function (value) {
        delete $scope.indicator
        delete $scope.responsable.indicator_id
        $scope.v.indicator.class = value ? 'has-warning' : ''
      },
    },
  }

  $scope.acOptionsPeople = {
    datasets: {
      display: 'cedula',
      templates: {
        suggestion: Handlebars.compile(`\
<div><strong>{{cedula}}</strong> · {{nombres}} {{apellidos}}</div>`)
      }
    },
    bloodhound: {
      prefetch: '/rest/people/responsables-disponibles',
      remote: { url: '/rest/people/responsables-disponibles' }
    },
  }

  $scope.acOptionsIndicator = {
    datasets: {
      display: 'indicator',
      templates: {
        suggestion: Handlebars.compile(`\
<div><strong>{{criterion.id}}.{{subcriterion.id}}.{{id}}</strong> · {{indicator}}</div>`)
      }
    },
    bloodhound: {
      prefetch: '/rest/indicators/disponibles',
      remote: { url: '/rest/indicators/disponibles' }
    },
  }

  $scope.init = function () {
    $http.get('/rest/responsables', $scope.responsable)
      .then(res => {
        $scope.responsables = res.data
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.addIndicator = function (responsable_id) {
    $scope.responsable = {
      _id: responsable_id
    }
    $('#indicadorModal').modal('show')
  }

  $scope.deleteIndicator = function (responsableID, indicatorID) {
    notify.swal({
      title: '¿Borrar indicador?',
      text: 'Los datos se borrarán permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete(`/rest/responsables/${responsableID}/indicators/${indicatorID}`)
        .then(res => {
          notify.success('Borrado')
          $scope.init()
        }, res => {
          console.error(res.data)
          notify.serverError()
        })
    })
  }

  $scope.submitIndicador = function () {

    if ($scope.v.indicator.class !== 'has-success') {
      notify.warning('No se han completado los campos requeridos')
      return
    }

    $http.put('/rest/responsables/add-indicator', $scope.responsable)
      .then(() => {
        $('#indicadorModal').modal('hide')
        notify.saved()
        location.reload();
        delete $scope.v.indicator.class
        delete $scope.indicator
        delete $scope.t.indicator_id
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.submit = function () {
    if ($scope.v.responsables.class !== 'has-success') {
      notify.warning('No se han completado los campos requeridos')
      return
    }
    $http.post('/rest/responsables', $scope.responsable)
      .then(res => {
        notify.saved()
        location.reload();
        $scope.init()
        $scope.v.responsables.class = ''
        delete $scope.person
        delete $scope.t.person_id
        delete $scope.responsable.person_id
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.delete = function (id) {
    notify.swal({
      title: '¿Borrar?',
      text: 'Los datos se borrarán permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete('/rest/responsables/' + id)
        .then(res => {
          notify.success('Borrado')
          $scope.init()
        }, res => {
          console.error(res.data)
          notify.serverError()
        })
    })
  }
}

function evaluatorsController($scope, $http) {

  $scope.responsable = { }
  $scope.evaluator = { }

  $scope.v = {
    evaluators: {
      class: '',
      selected: function (data) {
        $scope.evaluator.person_id = data._id
        $scope.person1 = `${data.cedula} · ${data.nombres} ${data.apellidos}`
        $scope.v.evaluators.class = 'has-success'
      },
      change: function (value) {
        delete $scope.person1
        delete $scope.evaluator.person_id
        $scope.v.evaluators.class = value ? 'has-warning' : ''
      },
    },
    responsable: {
      class: '',
      selected: function (data) {
        $scope.responsable.responsable_id = data._id
	if (data.person_id === $scope.responsable.person_id) {
	  $scope.v.evaluators.class = 'has-warning'
	  $scope.person2 = 'Un responsable no puede autoevaluarse'
	} else {
          $scope.person2 = `${data.person.cedula} · ${data.person.nombres} ${data.person.apellidos}`
          $scope.v.responsable.class = 'has-success'
	}
      },
      change: function (value) {
        delete $scope.person2
        delete $scope.responsable.responsable_id
        $scope.v.responsable.class = value ? 'has-warning' : ''
      },
    },
  }

  $scope.acOptionsPeople = {
    datasets: {
      display: 'cedula',
      templates: {
        suggestion: Handlebars.compile(`\
<div><strong>{{cedula}}</strong> · {{nombres}} {{apellidos}}</div>`)
      }
    },
    bloodhound: {
      prefetch: '/rest/people/evaluators-disponibles',
      remote: { url: '/rest/people/evaluators-disponibles' }
    },
  }

  $scope.acOptionsResponsable = {
    datasets: {
      display: 'putamierda',
      templates: {
        suggestion: Handlebars.compile(`\
<div>{{person.cedula}} · {{person.nombres}} {{person.apellidos}}</div>`)
      }
    },
    bloodhound: {
      prefetch: '/rest/responsables',
      remote: { url: '/rest/responsables' }
    },
  }

  $scope.init = function () {
    $http.get('/rest/evaluators')
      .then(res => {
        $scope.evaluators = res.data
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.addResponsable = function (evaluator_id, person_id) {
    $scope.responsable = { _id: evaluator_id, person_id: person_id }
    $('#responsableModal').modal('show')
  }

  $scope.deleteResponsable = function (evaluatorID, responsableID) {
    notify.swal({
      title: '¿Borrar responsable?',
      text: 'Los datos se borrarán permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete(`/rest/evaluators/${evaluatorID}/responsables/${responsableID}`)
        .then(res => {
          notify.success('Borrado')
          $scope.init()
        }, res => {
          console.error(res.data)
          notify.serverError()
        })
    })
  }

  $scope.submitResponsable = function () {
    if ($scope.v.responsable.class !== 'has-success') {
      notify.warning('No se han completado los campos requeridos')
      return
    }

    $http.put('/rest/evaluators/add-responsable', $scope.responsable)
      .then(() => {
        $('#responsableModal').modal('hide')
        notify.saved()
        location.reload();
        delete $scope.v.responsable.class
        delete $scope.responsable
        delete $scope.t.responsable_id
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }

  $scope.submit = function () {
    if ($scope.v.evaluators.class !== 'has-success') {
      notify.warning('No se han completado los campos requeridos')
      return
    }

    $http.post('/rest/evaluators', $scope.evaluator)
      .then(res => {
        notify.saved()
        location.reload();
        $scope.init()
        $scope.v.evaluators.class = ''
        delete $scope.person1
        delete $scope.t.person1_id
        delete $scope.responsable.person_id
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
  }
}

angular.module('configuration')
  .controller('people', peopleController)
  .controller('responsables', responsablesController)
  .controller('evaluators', evaluatorsController)
