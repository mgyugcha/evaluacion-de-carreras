function alertsController($scope,$http)
{
  $scope.alerts=[]
  
  $scope.init = function(){
    $http.get('/rest/alerts')
    .then(res=>{
      $scope.alerts=res.data
    },res=>{
      console.error(res.data)
      notify.serverError()
    })
  }


  $scope.submit=function()
  {
    let url = '/rest/alerts/' + ($scope.alert._id || '')
    let http = $scope.alert._id ? $http.put : $http.post
    http(url, $scope.alert)
    .then(res => {
      $('#alertModal').modal('hide')
      notify.saved()
      $scope.init()
      delete $scope.alert
    },res => {
      console.error(res.data)
      notify.serverError()
    })
  }

  $scope.delete=function(id)
  {
    notify.swal({
      title: '¿Borrar?',
      text: 'Los datos se borraran permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete('/rest/alerts/' + id)
      .then(res => {
        notify.success('Borrado')
        $scope.init()
      },res => {
        console.error(res.data)
        notify.serverError()
      })
    })
  }

  $scope.openModal = function (alert=null) {
    $scope.alert = alert ? angular.copy(alert) : {}
    $('#alertModal').modal('show')
  }

}
angular.module('configuration')
  .controller('alerts', alertsController)