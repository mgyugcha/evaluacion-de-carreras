function departmentsController($scope, $http) {
  $scope.departments = []

  /**
   * Inicia todos los departamentos de la base de datos.
   */
  $scope.init = function () {
    $http.get('/rest/departments')
      .then(res => {
	$scope.departments = res.data
      }, res=> {
	console.error(res.data)
	notify.serverError()
      }) 
  }

  /**
   * Abre el cuadro de dialogo modal. Esta función recibe un parametro
   * un objecto departamento. Si esta variable es null entonces abre
   * el modal para insertar en la base de datos, caso contrario se
   * abre el modal con los datos del departamento para editar.
   */
  $scope.openModal = function (department=null) {
    $scope.department = department ? angular.copy(department) : {}
    $('#departmentModal').modal('show')
  }

  /**
   * Me envia el formulario dependiendo si es para crear hace una
   * petición post y si es editar hace una peticion put. Cuando se
   * haya actualizado o creado se llama a la función init para iniciar
   * nuevamente todos los departamentos.
   */
  $scope.submit = function () {
    let url = '/rest/departments/' + ($scope.department._id || '')
    let http =  $scope.department._id ? $http.put : $http.post
    http(url, $scope.department)
      .then(res => {
	$('#departmentModal').modal('hide')
	notify.saved()
	$scope.init()
	delete $scope.department
      }, res => {
	console.error(res.data)
	notify.serverError()
      })
  }

  $scope.delete = function (id) {
    notify.swal({
      title: '¿Borrar?',
      text: 'Los datos se borraran permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete('/rest/departments/' + id)
	.then(res => {
	  notify.success('Borrado')
	  $scope.init()
	}, res => {
	  console.error(res.data)
	  notify.serverError()
	})
    })
  }
}

angular.module('configuration')
  .controller('departments', departmentsController)
