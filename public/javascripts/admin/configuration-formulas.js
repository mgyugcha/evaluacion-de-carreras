function formulasController($scope,$http){
  $scope.formulas = []

  $scope.init = function() {
    $http.get('/rest/formulas')
    .then(res=>{
      $scope.formulas=res.data
    }, res=>{
      console.error(res.data)
      notify.serverError()
    })
  }

  $scope.submit = function () {
    let url = '/rest/formulas/' + ($scope.formula._id || '')
    let http = $scope.formula._id ? $http.put : $http.post  
    http(url, $scope.formula)
    .then(res => {
      $('#formulasModal').modal('hide')
      notify.saved()
      $scope.init()
      delete $scope.formula
    }, res => {
      console.error(res.data)
      notify.serverError()
    }) 
  }

  $scope.delete = function (id) {
    notify.swal({
      title: '¿Borrar?',
      text: 'Los datos se borraran permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete('/rest/formulas/' + id)
      .then(res => {
        notify.success('Borrado')
        $scope.init()
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
    })
  }

  $scope.openModal = function (formula=null) {
    $scope.formula = formula ? angular.copy(formula) : {}
    $('#formulasModal').modal('show')
  }

}
angular.module('configuration')
  .controller('formulas', formulasController)