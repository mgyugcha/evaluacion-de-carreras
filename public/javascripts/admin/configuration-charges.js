function chargeController($scope,$http){
  $scope.charges = []

  $scope.init = function() {
    $http.get('/rest/charges')
    .then(res=>{
      $scope.charges=res.data
    }, res=>{
      console.error(res.data)
      notify.serverError()
    })
  }

  $scope.submit = function () {
    let url = '/rest/charges/' + ($scope.charge._id || '')
    let http = $scope.charge._id ? $http.put : $http.post  
    http(url, $scope.charge)
    .then(res => {
      $('#chargeModal').modal('hide')
      notify.saved()
      $scope.init()
      delete $scope.charge
    }, res => {
      console.error(res.data)
      notify.serverError()
    }) 
  }

  $scope.delete = function (id) {
    notify.swal({
      title: '¿Borrar?',
      text: 'Los datos se borraran permanentemente',
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete('/rest/charges/' + id)
      .then(res => {
        notify.success('Borrado')
        $scope.init()
      }, res => {
        console.error(res.data)
        notify.serverError()
      })
    })
  }

  $scope.openModal = function (charge=null) {
    $scope.charge = charge ? angular.copy(charge) : {}
    $('#chargeModal').modal('show')
  }

}
angular.module('configuration')
  .controller('charges', chargeController)