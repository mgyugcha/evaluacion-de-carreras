angular.module('evaluator', [])
  .controller('main', function ($scope, $http) {

    $scope.evaluator_id = null

    $scope.init = function (evaluator_id) {
      if (evaluator_id)
	$scope.evaluator_id = evaluator_id
      $http.get('/rest/evaluators/' + $scope.evaluator_id)
        .then(res => {
	  console.log(res.data)
          $scope.responsables = res.data.responsables
	  console.log($scope.responsables)
        }, res => {
          console.log(res.data)
          notify.serverError()
        })
    }

    $scope.openUploadModal = function (indicatorID) {
      $scope.indicatorID = indicatorID
      $scope.disableButton = false
    }

  })
  .controller('calificacion', function ($scope, $http) {
    $scope.init = function (responsable_id) {
      $http.get('/rest/responsables/' + responsable_id)
        .then(res => {
          $scope.responsable = res.data
        }, res => {
          console.error(res.data)
          notify.serverError()
        })
      $http.get('/rest/alerts')
	.then(res => {
	  $scope.alerts = res.data
	}, res => {
          console.error(res.data)
          notify.serverError()
	})
      $http.get('/rest/scales')
	.then(res => {
	  $scope.scales = res.data
	}, res => {
	  console.error(res.data)
          notify.serverError()
	})
    }

    $scope.abrirModalEvidencia = function (evidencia) {
      $scope.evidencia = evidencia
    }

    $scope.calificarEvidencia = function () {
      let calificacion = {
	alert_id: $scope.evidencia.calificacion.alert_id,
	descripcion: $scope.evidencia.calificacion.descripcion,
      }
      $http.post('/rest/evidence/' + $scope.evidencia._id + '/calificar', calificacion)
      	.then(res => {
	  notify.saved()
	  $('#calificarEvidencia').modal('hide')
      	}, res => {
          console.error(res.data)
          notify.serverError()
      	})
    }

    $scope.abrirModalIndicador = function (indicador) {
      $scope.indicador = indicador
    }

    $scope.calificarIndicador = function () {
      let calificacion = { date: new Date() }
      if ($scope.indicador.calificacion.scale_id) {
	calificacion.scale_id = $scope.indicador.calificacion.scale_id
      } else {
	calificacion.value = $scope.indicador.calificacion.value
      }
      //console.log($scope.calificacion)
      $http.post('/rest/indicators/' + $scope.indicador._id + '/calificar', calificacion)
      	.then(res => {
	  notify.saved()
	  $('#calificarIndicador').modal('hide')
      	}, res => {
          console.error(res.data)
          notify.serverError()
      	})      
    }

  })
