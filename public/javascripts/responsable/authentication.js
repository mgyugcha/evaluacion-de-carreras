const mainroute = '/responsable'

function loginController($scope, $http) {
  $scope.submit = function () {
    $http.post(`${mainroute}/login`, $scope.user)
      .then(res => {
        window.location = mainroute
      }, res => {
	console.error(res.data)
        if (res.status === 404) {
          notify.warning('Datos incorrecto', `\
Verifique que la cédula y la contraseña sean correctos`)
        } else {
          notify.serverError()
        }
      })
  }
}

angular.module('authentication', [])
  .controller('login', loginController)
