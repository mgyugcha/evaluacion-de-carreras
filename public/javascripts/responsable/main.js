angular.module('responsable', [])
  .controller('main', function ($scope, $http) {

    $scope.responsable_id = null

    $scope.init = function (responsable_id) {
      if (responsable_id)
        $scope.responsable_id = responsable_id
      $http.get('/rest/responsables/' + $scope.responsable_id)
        .then(res => {
          $scope.indicadores = res.data.indicadores
        }, res => {
          console.log(res.data)
          notify.serverError()
        })

      $http.get('/rest/alerts')
        .then(res => {
          $scope.alerts = res.data
        }, res => {
          console.error(res.data)
          notify.serverError()
        })
      $http.get('/rest/scales')
        .then(res => {
          $scope.scales = res.data
        }, res => {
          console.error(res.data)
          notify.serverError()
        })
    }

    $scope.openUploadModal = function (indicatorID) {
      $scope.indicatorID = indicatorID
      $scope.disableButton = false
    }

    $scope.upload = function () {
      $scope.disableButton = true
      $.ajax({
        url: '/rest/evidence/upload-file?indicator=' + $scope.indicatorID,
        type: 'POST',
        data: new FormData($('form')[0]),
        cache: false,
        contentType: false,
        processData: false,
      }).done(function() {
        notify.success('Archivo Subido')
        $scope.init()
        document.getElementById("archivo").value = "";
        $('#uploadModal').modal('hide')
      }).fail(function(e) {
        console.log(e)
        notify.serverError()
      })
    }

    $scope.deleteEvidence = function (evidenceID) {
      notify.swal({
        title: '¿Borrar evidencia?',
        text: 'Los datos se borrarán permanentemente',
        showCancelButton: true,
        confirmButtonClass: 'btn-danger',
        confirmButtonText: 'Borrar',
        closeOnConfirm: false
      }, () => {
        $http.delete(`/rest/evidence/${evidenceID}`)
          .then(res => {
            notify.success('Borrado')
            $scope.init()
          }, res => {
            console.error(res.data)
            notify.serverError()
          })
      })
    }

  })
